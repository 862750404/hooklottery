package com.example.hooklottery.bean

data class Token(
    var expired: String,
    var token: String
) {
    /**
     * 后台需求token需要空格拼接
     */
    fun splicingToken(): String = "X-Token $token"
}