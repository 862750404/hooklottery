package com.example.hooklottery.ui.activity

import android.content.Context
import android.content.Intent
import android.view.View
import com.drake.net.Post
import com.drake.net.utils.scopeDialog
import com.example.hooklottery.R
import com.example.hooklottery.base.EngineActivity
import com.example.hooklottery.bean.Token
import com.example.hooklottery.constant.Kv.kv
import com.example.hooklottery.constant.TOKEN
import com.example.hooklottery.databinding.ActivityLoginBinding
import com.example.hooklottery.model.LoginModel
import kotlinx.android.synthetic.main.activity_login.*

const val PASSWORD = "password"
const val USERNAME = "username"
const val REMEMBER_PWD_PREF = "rememberPwd"

class LoginActivity : EngineActivity<ActivityLoginBinding>(R.layout.activity_login) {
    private lateinit var model: LoginModel
    private var isRemember: Boolean = kv.encode(REMEMBER_PWD_PREF, false)

    override fun initData() {
        model = LoginModel()
        binding.m = model
        if (isRemember) {//设置【账号】与【密码】到文本框，并勾选【记住密码】
            model.password = kv.decodeString(PASSWORD, "")
            model.username = kv.decodeString(USERNAME, "")
            //model 动态更新数据
            model.notifyChange()
            cb.isChecked = true
        }
    }

    override fun initView() {
        binding.v = this
    }

    companion object {
        @JvmStatic
        fun forwardLogin(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tv_forget_password -> {

            }
            R.id.cb -> {
                if (cb.isChecked) {
                    kv.encode(REMEMBER_PWD_PREF, true)
                } else {
                    kv.encode(REMEMBER_PWD_PREF, false)
                }
            }
            R.id.tv_login -> {
                login()
            }
        }
    }

    private fun login() {
        if (model.getVerifyLogin()) return
        scopeDialog {
            val data = Post<Token>("login") {
                param("username", model.username)
                param("password", model.password)
            }.await()
            kv.encode(TOKEN, data.splicingToken())
            if (cb.isChecked) {
                kv.encode(REMEMBER_PWD_PREF, true)
                kv.encode(USERNAME, model.username)
                kv.encode(PASSWORD, model.password)
            } else {
                val key = arrayOf(USERNAME, PASSWORD, REMEMBER_PWD_PREF)
                kv.removeValuesForKeys(key);
            }
            finish()
        }
    }
}