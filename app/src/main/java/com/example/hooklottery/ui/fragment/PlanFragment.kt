package com.example.hooklottery.ui.fragment

import com.drake.brv.utils.models
import com.drake.brv.utils.setup
import com.drake.net.Get
import com.drake.net.utils.scope
import com.example.hooklottery.R
import com.example.hooklottery.base.EngineFragment
import com.example.hooklottery.component.net.toJsonArray
import com.example.hooklottery.databinding.FragmentPlanBinding
import com.example.hooklottery.model.LotteriesModel
import kotlinx.android.synthetic.main.fragment_plan.*

class PlanFragment : EngineFragment<FragmentPlanBinding>(R.layout.fragment_plan) {
    override fun initView() {
        rv_plan.setup {
            addType<LotteriesModel>(R.layout.item_plan_lottery)
        }
    }

    override fun initData() {
        getLotteries()
    }

    private fun getLotteries() {
        scope {
            val data = Get<String>("lotteries").await().toJsonArray<LotteriesModel>()
            rv_plan.models = data
        }
    }
}