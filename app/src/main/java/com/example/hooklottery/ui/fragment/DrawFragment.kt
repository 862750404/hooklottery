package com.example.hooklottery.ui.fragment

import com.example.hooklottery.R
import com.example.hooklottery.base.EngineFragment
import com.example.hooklottery.databinding.FragmentPlanBinding

class DrawFragment : EngineFragment<FragmentPlanBinding>(R.layout.fragment_draw) {
    override fun initView() {
    }

    override fun initData() {
    }
}