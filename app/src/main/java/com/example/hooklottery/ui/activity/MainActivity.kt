package com.example.hooklottery.ui.activity

import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.hooklottery.R
import com.example.hooklottery.base.EngineActivity
import com.example.hooklottery.component.FragmentPagerAdapter
import com.example.hooklottery.databinding.ActivityMainBinding
import com.example.hooklottery.ui.fragment.DrawFragment
import com.example.hooklottery.ui.fragment.MineFragment
import com.example.hooklottery.ui.fragment.PlanFragment
import com.example.hooklottery.ui.fragment.ProgramFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : EngineActivity<ActivityMainBinding>(R.layout.activity_main) {

    override fun initData() {
    }

    override fun initView() {

        val fragments =
            mutableListOf<Fragment>(
                PlanFragment(),
                ProgramFragment(),
                DrawFragment(),
                MineFragment()
            )
        vp_content.adapter = FragmentPagerAdapter(fragments)
        vp_content.offscreenPageLimit = 4
        bnv_main.itemIconTintList = null
        val states = arrayOf(
            intArrayOf(-android.R.attr.state_checked),
            intArrayOf(android.R.attr.state_checked)
        )
        val colors = intArrayOf(
            ContextCompat.getColor(this, R.color.bottom_navigation_normal),
            ContextCompat.getColor(this, R.color.bottom_navigation_checked)
        )
        val csl = ColorStateList(states, colors)
        bnv_main.itemTextColor = csl
        bnv_main.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.plan -> vp_content.setCurrentItem(0, false)
                R.id.program -> vp_content.setCurrentItem(1, false)
                R.id.draw -> vp_content.setCurrentItem(2, false)
                R.id.mine -> vp_content.setCurrentItem(3, false)
            }
            true
        }
    }

}