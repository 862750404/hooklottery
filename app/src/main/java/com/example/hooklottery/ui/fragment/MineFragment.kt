package com.example.hooklottery.ui.fragment

import com.example.hooklottery.R
import com.example.hooklottery.base.EngineFragment
import com.example.hooklottery.databinding.FragmentPlanBinding

class MineFragment : EngineFragment<FragmentPlanBinding>(R.layout.fragment_mine) {
    override fun initView() {
    }

    override fun initData() {
    }
}