package com.example.hooklottery.model

import androidx.databinding.BaseObservable
import com.example.hooklottery.R
import com.example.hooklottery.base.App
import com.example.hooklottery.component.toast
import com.example.hooklottery.constant.Kv
import com.example.hooklottery.constant.Kv.kv
import com.example.hooklottery.ui.activity.PASSWORD
import com.example.hooklottery.ui.activity.USERNAME


class LoginModel(
    var domain: String = "kcz11.com",
) : BaseObservable() {
    var password: String = ""
    var username: String = ""

    /**
     * 校验登录
     */
    fun getVerifyLogin(): Boolean {
        when {
            username.isBlank() -> App.app.toast(R.string.please_input_username)
            password.isBlank() -> App.app.toast(R.string.please_input_password)
            else -> return false
        }
        return true
    }
}