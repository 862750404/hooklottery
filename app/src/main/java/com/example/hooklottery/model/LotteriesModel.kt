package com.example.hooklottery.model

data class LotteriesModel(
    var code: String,
    var icon: String,
    var id: Int,
    var issue: Issue,
    var name: String,
    var play_names: List<String>,
    var type: String
) {
    data class Issue(
        var fp_time: String,
        var issue: String,
        var last_issue: String,
        var last_numbers: String,
        var last_open_time: String,
        var now_time: String,
        var open_time: String,
        var start_time: String,
        var status: Int
    )
}