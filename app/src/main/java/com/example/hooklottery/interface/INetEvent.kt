package com.example.hooklottery.`interface`

interface INetEvent {
    fun onNetChange(netWorkState: Int)
}