package com.example.hooklottery.component.net

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


inline fun <reified T> String.toJsonArray(): MutableList<T> {
    return Gson().fromJson(this, TypeToken.getParameterized(List::class.java, T::class.java).type)
}