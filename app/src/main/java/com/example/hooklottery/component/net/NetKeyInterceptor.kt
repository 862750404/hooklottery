package com.example.hooklottery.component.net

import com.yanzhenjie.kalle.RequestMethod
import com.yanzhenjie.kalle.Response
import com.yanzhenjie.kalle.connect.Interceptor
import com.yanzhenjie.kalle.connect.http.Chain
import com.yanzhenjie.kalle.simple.SimpleBodyRequest
import com.yanzhenjie.kalle.simple.SimpleUrlRequest

class NetKeyInterceptor : Interceptor {
    override fun intercept(chain: Chain): Response {
        var request = chain.request()

        // 除非是Download*函数否则都仅有两种Request: SimpleBodyRequest/SimpleUrlRequest
        request = when (request.method()) {
            RequestMethod.POST -> {
                val copyParams =
                    request.copyParams().builder().add("appkey", "85b81a25b4094ace").build() // 添加一个参数
                SimpleBodyRequest.newBuilder(request.url(), request.method())
                    .setParams(copyParams)
                    .setHeaders(request.headers())
                    .tag(request.tag())
                    .build()
            }
            RequestMethod.GET -> {
                val copyParams =
                    request.copyParams().builder().add("appkey", "85b81a25b4094ace").build()
                SimpleUrlRequest.newBuilder(request.url(), request.method())
                    .setParams(copyParams)
                    .setHeaders(request.headers())
                    .tag(request.tag())
                    .build()
            }
            else -> request
        }
        return chain.proceed(request)
    }
}