package com.example.hooklottery.component.net

import com.drake.net.error.RequestParamsException
import com.drake.net.error.ResponseException
import com.drake.net.error.ServerResponseException
import com.google.gson.Gson
import com.yanzhenjie.kalle.Request
import com.yanzhenjie.kalle.Response
import com.yanzhenjie.kalle.exception.ParseError
import com.yanzhenjie.kalle.simple.Converter
import org.json.JSONObject
import java.lang.reflect.Type

@Suppress("UNCHECKED_CAST")
class DefaultConvert(
    private val status: String = "status",
    private val message: String = "message",
    private val success: String = "0"
) : Converter {

    @Suppress("UNREACHABLE_CODE")
    override fun <S> convert(
        succeed: Type,
        request: Request,
        response: Response,
        cache: Boolean
    ): S? {
        var body = response.body().string()
        response.log = body  // 日志记录响应信息
        val code = response.code()
        when {
            code in 200..299 -> { // 请求成功
                val jsonObject = JSONObject(body) // 获取JSON中后端定义的错误码和错误信息
                if (jsonObject.getString(this.status) == success) { // 对比后端自定义错误码
                    val data = jsonObject.getString("data")
                    return if (succeed === String::class.java) data as S else data.parseBody(succeed)
                } else { // 错误码匹配失败, 开始写入错误异常
                    throw ResponseException(code, jsonObject.getString(message), request, body)
                }
            }
            code in 400..499 -> throw RequestParamsException(code, request) // 请求参数错误
            code >= 500 -> throw ServerResponseException(code, request) // 服务器异常错误
            else -> throw ParseError(request)
        }
    }

    private val data = Gson()

    /**
     * 解析字符串数据
     * 一般用于解析JSON
     * @param succeed 请求函数定义的泛型, 例如一般的Moshi/Gson解析数据需要使用
     * @receiver 原始字符串
     */
    private fun <S> String.parseBody(succeed: Type): S? {
        return data.fromJson(this, succeed)
    }
}