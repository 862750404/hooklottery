package com.example.hooklottery.widget

import android.view.Gravity
import android.view.View
import com.drake.tooltip.toast
import com.example.hooklottery.R
import com.example.hooklottery.base.App
import kotlinx.android.synthetic.main.layout_toast_error.view.*

object MyToast {
    var app = App.app
    fun toastSuccess(msg: String) {
        app.toast() {
            setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 0)
            // ProgressBar(context) // 自定义视图显示加载进度
            View.inflate(app, R.layout.layout_toast_success, null)
                .apply { tv_msg.text = msg }
        }
    }

    fun toastError( msg: String) {
        app.toast() {
            setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 0)
            // ProgressBar(context) // 自定义视图显示加载进度
            View.inflate(app, R.layout.layout_toast_error, null)
                .apply { tv_msg.text = msg }
        }
    }
}