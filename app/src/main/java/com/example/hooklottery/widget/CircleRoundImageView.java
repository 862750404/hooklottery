package com.example.hooklottery.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.example.hooklottery.R;


public class CircleRoundImageView extends androidx.appcompat.widget.AppCompatImageView {
    private Paint mPaint;
    private RectF mRectF;
    private Path mPath;
    private int mCircleRoundRadius;

    public CircleRoundImageView(Context context) {
        this(context, null);
    }

    public CircleRoundImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleRoundImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleRoundImageView);
        mCircleRoundRadius = typedArray.getDimensionPixelOffset(R.styleable.CircleRoundImageView_circle_round_radius, 20);
        typedArray.recycle();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mRectF == null) {
            mRectF = new RectF(0, 0, getWidth(), getHeight());
        }
        mPath.addRoundRect(mRectF, mCircleRoundRadius, mCircleRoundRadius, Path.Direction.CW);
        canvas.clipPath(mPath);
        super.onDraw(canvas);
    }
}