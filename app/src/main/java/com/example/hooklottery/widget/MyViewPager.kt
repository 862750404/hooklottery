package com.example.hooklottery.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager
import com.example.hooklottery.R
import com.example.hooklottery.urils.NetworkHelper

/**
 * 可以禁止滑动的ViewPager
 */
class MyViewPager(
    context: Context,
    attrs: AttributeSet?
) : ViewPager(context, attrs) {

    private var scrollEnabled: Boolean

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return if (scrollEnabled && NetworkHelper.isNetworkAvailable(context)) {
            try {
                super.onInterceptTouchEvent(ev)
            } catch (e: Exception) {
                e.printStackTrace()
                false
            }
        } else {
            false
        }
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return if (scrollEnabled) {
            super.onTouchEvent(ev)
        } else {
            true
        }
    }

    fun setCanScroll(canScroll: Boolean) {
        scrollEnabled = canScroll
    }

    init {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.MyViewPager)
        scrollEnabled = ta.getBoolean(R.styleable.MyViewPager_canScroll, true)
        ta.recycle()
    }
}