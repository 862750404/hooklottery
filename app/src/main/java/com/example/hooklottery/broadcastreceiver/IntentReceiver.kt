package com.example.hooklottery.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.widget.Toast

class IntentReceiver() : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT < 23) {//sdk <23
            val networkInfo = manager.activeNetworkInfo
            if (networkInfo != null && networkInfo.isAvailable) {
                if (networkInfo.type == ConnectivityManager.TYPE_WIFI) {//WIFI
                    //   Log.d("日志", "(IntentReceiver.kt:18)    wifi")
                } else if (networkInfo.type == ConnectivityManager.TYPE_MOBILE) {//移动数据
                    //  Log.d("日志", "(IntentReceiver.kt:22)    移动数据")
                }
            } else {
                Toast.makeText(context, "无网络连接", Toast.LENGTH_SHORT).show()
            }
        } else {
            val network = manager.activeNetwork
            if (network != null) {
                val nc = manager.getNetworkCapabilities(network)
                if (nc != null) {
                    if (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {//WIFI
                        //  Log.d("日志", "(IntentReceiver.kt:32)    wifi")
                    } else if (nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {//移动数据
                        //   Log.d("日志", "(IntentReceiver.kt:34)    移动数据")
                    }
                }
            } else {
                Toast.makeText(context, "无网络连接", Toast.LENGTH_SHORT).show()
            }
        }
    }
}