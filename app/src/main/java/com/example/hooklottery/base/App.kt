package com.example.hooklottery.base

import android.app.Application
import com.drake.brv.utils.BRV

import com.drake.net.initNet
import com.drake.statelayout.StateConfig
import com.example.hooklottery.R
import com.example.hooklottery.component.net.DefaultConvert
import com.example.hooklottery.constant.Config
import com.example.hooklottery.constant.Kv
import com.example.hooklottery.constant.TOKEN
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.MaterialHeader
import com.scwang.smart.refresh.layout.SmartRefreshLayout
import com.tencent.mmkv.MMKV
import com.example.hooklottery.BR

import java.util.concurrent.TimeUnit

class App : Application() {

    companion object {
        lateinit var app: App
    }

    override fun onCreate() {
        app = this
        super.onCreate()
        val rootDir: String = MMKV.initialize(this)
        initNet(Config.host) {
            converter(DefaultConvert())
            addHeader("Authorization", Kv.kv.decodeString(TOKEN))
            setLogRecord(true) // 日志记录器
            connectionTimeout(1, TimeUnit.MINUTES)
            readTimeout(1, TimeUnit.MINUTES)
        }


        // RecyclerView使用MVVM初始化
        BRV.modelId = BR.m
        // 全局缺省页
        StateConfig.apply {
            errorLayout = R.layout.layout_error
            emptyLayout = R.layout.layout_empty
            loadingLayout = R.layout.layout_loading
        }

        // 下拉刷新上拉加载
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, _ ->
            MaterialHeader(context)
        }
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
            ClassicsFooter(context)
        }

    }
}