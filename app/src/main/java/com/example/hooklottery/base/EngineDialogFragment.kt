package com.example.hooklottery.base

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.drake.logcat.LogCat

abstract class EngineDialogFragment<B : ViewDataBinding>() : DialogFragment(), OnClickListener {

    lateinit var binding: B

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val adjustView = view ?: dialog?.findViewById<ViewGroup>(android.R.id.content)?.getChildAt(0) ?: return
        binding = DataBindingUtil.bind(adjustView)!!
        @Suppress("DEPRECATION")
        try {
            initView()
            initData()
        } catch (e: Exception) {
            LogCat.e("日志", "初始化失败")
            e.printStackTrace()
        }
    }

    abstract fun initView()
    abstract fun initData()
    override fun onClick(v: View) {}

}
