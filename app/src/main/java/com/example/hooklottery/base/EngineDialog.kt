package com.example.hooklottery.base

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class EngineDialog<B : ViewDataBinding>(context: Context) : Dialog(context),
    OnClickListener {

    lateinit var binding: B

    protected abstract fun initView()

    protected abstract fun initData()

    override fun onClick(v: View) {
    }

    override fun setContentView(layoutResID: Int) {
        binding = DataBindingUtil.inflate(layoutInflater, layoutResID, null, false)
        setContentView(binding.root)
        init()
    }

    open fun init() {
        try {
            initView()
            initData()
        } catch (e: Exception) {
            Log.e("日志", "初始化失败")
            e.printStackTrace()
        }
    }
}
