package com.example.hooklottery.base

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.drake.logcat.LogCat
import com.drake.statusbar.immersive
import com.example.hooklottery.broadcastreceiver.IntentReceiver
import org.greenrobot.eventbus.EventBus

abstract class EngineActivity<B : ViewDataBinding>(@LayoutRes contentLayoutId: Int = 0) :
    AppCompatActivity(contentLayoutId),View.OnClickListener {
    private val onBackPressInterceptors = ArrayList<() -> Boolean>()

    lateinit var binding: B
    lateinit var rootView: View

    // 全局网络广播监听
    var intentReceiver = IntentReceiver()

    override fun setContentView(layoutResID: Int) {
        rootView = layoutInflater.inflate(layoutResID, null)
        setContentView(rootView)
        binding = DataBindingUtil.bind(rootView)!!
        init()
    }

    open fun init() {
        immersive()
        var filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        this.registerReceiver(intentReceiver, filter)
        try {
            initView()
            initData()
        } catch (e: Exception) {
            LogCat.e("初始化失败")
            e.printStackTrace()
        }

    }

    abstract fun initData()

    abstract fun initView()

    /**
     * 返回键事件
     * @param block 返回值表示是否拦截事件
     */
    fun onBackPressed(block: () -> Boolean) {
        onBackPressInterceptors.add(block)
    }

    //打开event bus 注册开关
    protected open fun isRegisterEventBus(): Boolean {
        return false
    }

    override fun onStart() {
        super.onStart()
        if (isRegisterEventBus()) {
            EventBus.getDefault().register(this)
        }
    }


    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        unregisterReceiver(intentReceiver);
        super.onDestroy();
    }

    override fun onClick(v: View) {

    }
}