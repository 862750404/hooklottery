package com.example.hooklottery.constant

import com.example.hooklottery.BuildConfig


object Config {
    var host: String = localHosts()
    private fun localHosts(): String {
        return when (BuildConfig.BUILD_TYPE) {
            "debug" -> BuildConfig.HTTP_BASE
            "release" -> BuildConfig.HTTP_BASE
            else -> BuildConfig.HTTP_BASE
        }
    }
}